package grade;

import java.util.Scanner;

public class Grade {

    public static void main(String[] args) {
        System.out.print("Please EnterScore : ");
        Scanner Sc = new Scanner (System.in);
        int score = Sc.nextInt();
        System.out.print("Your Grade : ");
        
        if (score >=80){
            System.out.println("A");
        }else if (score>=75){
            System.out.println("B+");
        }else if (score>=70){
            System.out.println("B");
        }else if (score>=65){
            System.out.println("C+");
        }else if (score>=60){
            System.out.println("C");
        }else if (score>=55){
            System.out.println("D+");
        }else if (score>=50){
            System.out.println("D");
        }else{
            System.out.println("E");
        }
    }
    
}
